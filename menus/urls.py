from rest_framework import routers

from menus.views import MenuViewSet, CategoryViewSet, ProductViewSet

router = routers.DefaultRouter()

urlpatterns = []
router.register('menu', MenuViewSet, basename='menu_crud')
router.register('category', CategoryViewSet, basename='category_crud')
router.register('product', ProductViewSet, basename='product_crud')

urlpatterns += router.urls
