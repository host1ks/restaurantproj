from django_tenants.utils import get_tenant_model
from django_tenants.utils import tenant_context
from rest_framework import serializers

from menus.models import Menu, Product, Category


class BaseSerializer(serializers.Serializer):
    def create(self, validated_data):
        tenant = get_tenant_model().objects.get(owner_id=self.context.get('request').user.id)
        with tenant_context(tenant):
            return self.Meta.model.objects.create(**validated_data)


class MenuSerializer(BaseSerializer, serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = '__all__'


class CategorySerializer(BaseSerializer, serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class ProductSerializer(BaseSerializer, serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class CategoryAPISerializer(serializers.ModelSerializer):
    product = ProductSerializer(many=True)

    class Meta:
        model = Category
        fields = '__all__'


class CategoryForMenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class MenuAPISerializer(serializers.ModelSerializer):
    category = CategoryForMenuSerializer(many=True)

    class Meta:
        model = Menu
        fields = '__all__'
