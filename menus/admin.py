from django.contrib import admin

from menus.models import Menu, Category, Product

admin.site.register(Menu)
admin.site.register(Category)
admin.site.register(Product)
