from django_tenants.utils import get_tenant_model, tenant_context
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from menus.models import Menu, Category, Product
from menus.serializers import (
    CategorySerializer,
    MenuSerializer,
    ProductSerializer,
    CategoryAPISerializer,
    MenuAPISerializer
)


class DefaultViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]

    @staticmethod
    def get_tenant(user_id):
        return get_tenant_model().objects.get(owner_id=user_id)

    def list(self, request, *args, **kwargs):
        with tenant_context(self.get_tenant(self.request.user.id)):
            return super(DefaultViewSet, self).list(request, *args, **kwargs)

    def get_object(self):
        with tenant_context(self.get_tenant(self.request.user.id)):
            return super(DefaultViewSet, self).get_object()

    def update(self, request, *args, **kwargs):
        with tenant_context(self.get_tenant(self.request.user.id)):
            return super(DefaultViewSet, self).update(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        with tenant_context(self.get_tenant(self.request.user.id)):
            return super(DefaultViewSet, self).destroy(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        with tenant_context(self.get_tenant(self.request.user.id)):
            return super(DefaultViewSet, self).create(request, *args, **kwargs)


class MenuViewSet(DefaultViewSet):
    serializer_class = MenuSerializer
    queryset = Menu.objects.all()


class CategoryViewSet(DefaultViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()


class ProductViewSet(DefaultViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class MenuAPIViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = MenuAPISerializer
    queryset = Menu.objects.all()


class CategoryAPIViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = CategoryAPISerializer
    queryset = Category.objects.all()


class ProductAPIViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
