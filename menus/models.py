from django.db import models


class BaseModel(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class Menu(BaseModel):
    pass


class Category(BaseModel):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE, null=True, blank=True, related_name='category')

    class Meta:
        verbose_name_plural = 'Categories'


class Product(BaseModel):
    content = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True, related_name='product')
