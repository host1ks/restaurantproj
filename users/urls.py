from django.urls import path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView

from users.views import LoginView, SignupView, RetrieveUser

router = routers.DefaultRouter()

urlpatterns = [
    path('login/', LoginView.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('signup/', SignupView.as_view(), name='auth_register'),

]
router.register('', RetrieveUser, basename='retrieve_user')

urlpatterns += router.urls
