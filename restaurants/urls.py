from rest_framework import routers

from restaurants.views import RestaurantViewSet

router = routers.DefaultRouter()

urlpatterns = []
router.register('', RestaurantViewSet, basename='retrieve_user')

urlpatterns += router.urls
