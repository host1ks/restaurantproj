from rest_framework import serializers
from tenant_users.tenants.tasks import provision_tenant

from restaurants.models import Restaurant


class RestaurantSerializer(serializers.ModelSerializer):
    domain_url = serializers.CharField(max_length=255)

    def create(self, validated_data):
        if Restaurant.objects.filter(owner=self.context['request'].user).exists():
            raise serializers.ValidationError(
                {'restaurant': 'User may have one and only one restaurant'}
            )
        provision_tenant(
            validated_data['name'],
            validated_data['domain_url'],
            self.context['request'].user.email
        )
        instance = Restaurant.objects.get(owner=self.context['request'].user)
        instance.address = validated_data.get('address')
        instance.save()
        return instance

    class Meta:
        model = Restaurant
        fields = ('name', 'address', 'domain_url')
