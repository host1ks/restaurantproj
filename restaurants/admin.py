from django.contrib import admin
from django_tenants.admin import TenantAdminMixin
from tenant_users.permissions.models import UserTenantPermissions

from restaurants.models import Restaurant

admin.site.register(UserTenantPermissions)


@admin.register(Restaurant)
class ClientAdmin(TenantAdminMixin, admin.ModelAdmin):
    list_display = ('name', 'address')
