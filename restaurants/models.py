from django.db import models
from django_tenants.models import DomainMixin
from tenant_users.tenants.models import TenantBase


class Restaurant(TenantBase):
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255, blank=True)

    auto_drop_schema = True

    def __str__(self):
        return self.name

    def delete(self, force_drop=True, *args, **kwargs):
        super().delete(force_drop, *args, **kwargs)


class Domain(DomainMixin):
    pass
