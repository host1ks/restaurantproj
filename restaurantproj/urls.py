from django.contrib import admin
from django.urls import path
from rest_framework import routers

from menus.views import MenuAPIViewSet, CategoryAPIViewSet, ProductAPIViewSet

urlpatterns = [
    path('admin/', admin.site.urls),
]

router = routers.DefaultRouter()

router.register('menu', MenuAPIViewSet, basename='api_menu_crud')
router.register('category', CategoryAPIViewSet, basename='api_category_crud')
router.register('product', ProductAPIViewSet, basename='api_product_crud')

urlpatterns += router.urls
